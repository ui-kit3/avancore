import React, { FC } from 'react';
import style from './button-svg.module.css';

export type TPropsButtonSVG = {
  onClick?: () => void | (() => string);
  nameButton: string;
  variant: 'deleteCell' | 'editCell' | 'editExecutor' | 'save' | 'buttonSum';
  title: string;
};

export const ButtonSVG: FC<TPropsButtonSVG> = ({
  onClick,
  variant,
  nameButton,
  title,
}) => {
  const deleteClick = () => {
    if (onClick) onClick();
  };

  return (
    <button
      role="button-svg"
      type="button"
      onClick={deleteClick}
      className={`button-table ${style[variant]}`}
      aria-label="button"
      name={nameButton}
      title={title}
    />
  );
};
