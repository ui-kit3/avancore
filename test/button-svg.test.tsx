import React from 'react';
import { render, screen } from '@testing-library/react';
import { ButtonSVG } from '../src/components/buttons/button-svg/button-svg';

describe('ButtonSVG', () => {
  describe('Should be rendered correctly', () => {
    test('default', () => {
      render(
        <ButtonSVG
          nameButton="ButtonSVG"
          variant="deleteCell"
          title="Кнопка значок"
          onClick={() => console.log('Кнопка нажата')}
        />
      );
      const button = screen.getByRole('button-svg');
      expect(button?.classList.contains('deleteCell')).toBeTruthy();
    });
  });
});
