module.exports = {
  moduleNameManager: {
    '\\.(css)$': 'identity-obj-proxy',
  },
  transform: {
    // Добавьте этот трансформатор для CSS файлов
    '^.+\\.css$': 'jest-css-modules-transform',
    '^.+\\.tsx?$': 'ts-jest',
  },
};
