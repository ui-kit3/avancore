import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import {
  ButtonSVG,
  TPropsButtonSVG,
} from '../src/components/buttons/button-svg/button-svg';

const meta: Meta = {
  title: 'ButtonSVG',
  component: ButtonSVG,
  argTypes: {
    children: {
      control: {
        type: 'text',
      },
    },
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: StoryFn<TPropsButtonSVG> = args => <ButtonSVG {...args} />;

// By passing using the Args format for exported stories, you can control the props for a component for reuse in a test
// https://storybook.js.org/docs/react/workflows/unit-testing
export const Default = Template.bind({});

Default.args = {
  title: 'Кнопка в виде картинки',
  onClick: alert('Кнопка работает'),
  variant: 'deleteCell',
};
